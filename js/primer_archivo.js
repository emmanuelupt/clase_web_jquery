$(document).ready(function(){

    /*
    $("#mi-primer-form").submit(function(e){
        e.preventDefault();
        alert();
    });
    */
    $( "#guardar" ).click(function() {

        var apellidoPaterno = $("#apellido-paterno").val();
        var apellidoMaterno = $("#apellido-materno").val();
        var nombre = $("#nombre").val();
        var direccion = $("#direccion").val();
        var cp = $("#cp").val();
        var sexo = $("#sexo").val();
        var descripcion = $("#descripcion").val();
        var telefono = $("#tel").val();
        var correo = $("#correo").val();
        var pass1 = $("#password").val();
        var pass2 = $("#password-2").val();

        if(apellidoPaterno == "" || apellidoPaterno == null){
            alert("Ingresa el apellido Paterno");
            $("#apellido-paterno").focus();
            return false;
        }

        if(apellidoMaterno == "" || apellidoMaterno == null){
            alert("Ingresa el apellido Materno");
            $("#apellido-materno").focus();
            return false;
        }

        if(nombre == "" || nombre == null){
            alert("Ingresa el apellido Nombre");
            $("#nombre").focus();
            return false;
        }

        if(direccion == "" || direccion == null){
            alert("Ingresa el direccion");
            $("#direccion").focus();
            return false;
        }

        if(cp == "" || cp == null){
            alert("Ingresa el cp");
            $("#cp").focus();
            return false;
        }

        if(sexo == "" || sexo == null){
            alert("Ingresa el sexo");
            $("#sexo").focus();
            return false;
        }

        if(descripcion == "" || descripcion == null){
            alert("Ingresa el descripcion");
            $("#descripcion").focus();
            return false;
        }

        if(telefono == "" || telefono == null){
            alert("Ingresa el telefono");
            $("#tel").focus();
            return false;
        }

        if(correo == "" || correo == null){
            alert("Ingresa el correo");
            $("#correo").focus();
            return false;
        }

        if((pass1 == "" || pass1 == null) && (pass2 == "" || pass2 == null)){
            alert("Falta alguna de las dos contraseñas");
            $("#password").focus();
            return false;
        }

        if(pass1 != pass2){
            alert("Las contraseñas deben ser iguales");
            $("#password").focus();
            return false;
        }

        $("#mi-primer-form").submit();
    });

});

